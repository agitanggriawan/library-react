import React, { useContext, useEffect, useState } from 'react';
import { Grid, Paper, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {
  CREATE_BOOKSHELF,
  BOOKSHELF,
  UPDATE_BOOKSHELF
} from '../../graphql/Bookshelf';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const BookshelfForm = props => {
  const { history, query, mutate, isAdd, match } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [bookshelf, setBookshelf] = useState({});

  useEffect(() => {
    if (match.params.id) {
      setGlobalLoading(true);
      const getBookshelf = async () => {
        const {
          data: { bookshelf }
        } = await query({
          query: BOOKSHELF,
          variables: {
            id: match.params.id
          },
          fetchPolicy: 'no-cache'
        });

        setGlobalLoading(false);
        setBookshelf(bookshelf);
      };
      getBookshelf();
    }
  }, [match.params.id, query, setGlobalLoading]);

  const AddPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Rak Buku', url: '/bookshelf' }}
        tertiary={{ title: 'Tambah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            initialValues={{
              name: ''
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { createBookshelf }
                } = await mutate({
                  mutation: CREATE_BOOKSHELF,
                  variables: { data: { name } },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setSnack({
                  variant: 'success',
                  message: `${createBookshelf.name} Berhasil disimpan`,
                  opened: true
                });
                setGlobalLoading(false);
                resetForm();
                history.replace('/bookshelf');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Rak Buku"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/bookshelf')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  const EditPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Rak Buku', url: '/bookshelf' }}
        tertiary={{ title: 'Ubah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              name: bookshelf.name
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { updateBookshelf }
                } = await mutate({
                  mutation: UPDATE_BOOKSHELF,
                  variables: {
                    id: match.params.id,
                    data: {
                      name
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setGlobalLoading(false);
                setSnack({
                  variant: 'success',
                  message: `${updateBookshelf.name} Berhasil diubah`,
                  opened: true
                });
                resetForm();
                history.replace('/bookshelf');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Rak Buku"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        UPDATE
                      </Button>
                      <Button
                        onClick={() => history.replace('/bookshelf')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  return <div>{isAdd ? <AddPage /> : <EditPage />}</div>;
};

export default BookshelfForm;
