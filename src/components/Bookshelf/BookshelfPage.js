import React, { useState, useEffect, useContext } from 'react';
import GlobalContext from '../../utils/GlobalContext';
import { BOOKSHELFS } from '../../graphql/Bookshelf';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';

const BookshelfPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [bookshelf, setBookshelf] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Nama', field: 'name' },
    {
      title: 'Tanggal Dibuat',
      field: 'created_at',
      width: 500,
      type: 'datetime'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    setGlobalLoading(true);
    const getBookshelfs = async () => {
      const {
        data: { bookshelfs }
      } = await query({
        query: BOOKSHELFS,
        fetchPolicy: 'no-cache'
      });
      setBookshelf(bookshelfs);
    };

    getBookshelfs();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={bookshelf}
        title="Rak Buku"
        addUrl="/bookshelf/new"
        setOpen={setOpen}
        setTempData={setTempData}
        editUrl="/bookshelf/edit"
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Rak Buku"
        mappingRows={{
          id: 'ID Rak Buku',
          name: 'Nama Rak Buku',
          created_at: 'Tanggal Dibuat'
        }}
      />
    </div>
  );
};

export default BookshelfPage;
