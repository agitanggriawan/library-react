import React, { useContext, useEffect, useState } from 'react';
import { Grid, Paper, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { CREATE_CLASS, CLASS, UPDATE_CLASS } from '../../graphql/Class';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const ClassForm = props => {
  const { history, query, mutate, isAdd, match } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [klass, setClass] = useState({});

  useEffect(() => {
    if (match.params.id) {
      setGlobalLoading(true);
      const getClass = async () => {
        const { data } = await query({
          query: CLASS,
          variables: {
            id: match.params.id
          },
          fetchPolicy: 'no-cache'
        });

        setGlobalLoading(false);
        setClass(data.class);
      };
      getClass();
    }
  }, [match.params.id, query, setGlobalLoading]);

  const AddPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Kelas', url: '/class' }}
        tertiary={{ title: 'Tambah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            initialValues={{
              grade: ''
            }}
            validationSchema={Yup.object().shape({
              grade: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ grade }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { createClass }
                } = await mutate({
                  mutation: CREATE_CLASS,
                  variables: { data: { grade } },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setSnack({
                  variant: 'success',
                  message: `${createClass.grade} Berhasil disimpan`,
                  opened: true
                });
                setGlobalLoading(false);
                resetForm();
                history.replace('/class');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="grade"
                        label="Nama Kelas"
                        name="grade"
                        onChange={handleChange}
                        value={values.grade}
                      />
                      {errors.grade && touched.grade && (
                        <div style={{ color: 'red' }}>{errors.grade}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/class')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  const EditPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Kelas', url: '/class' }}
        tertiary={{ title: 'Ubah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              grade: klass.grade
            }}
            validationSchema={Yup.object().shape({
              grade: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ grade }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { updateClass }
                } = await mutate({
                  mutation: UPDATE_CLASS,
                  variables: {
                    id: match.params.id,
                    data: {
                      grade
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setGlobalLoading(false);
                setSnack({
                  variant: 'success',
                  message: `${updateClass.grade} Berhasil diubah`,
                  opened: true
                });
                resetForm();
                history.replace('/class');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="grade"
                        label="Nama Kelas"
                        name="grade"
                        onChange={handleChange}
                        value={values.grade}
                      />
                      {errors.grade && touched.grade && (
                        <div style={{ color: 'red' }}>{errors.grade}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        UPDATE
                      </Button>
                      <Button
                        onClick={() => history.replace('/class')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  return <div>{isAdd ? <AddPage /> : <EditPage />}</div>;
};

export default ClassForm;
