import React, { useState, useEffect, useContext } from 'react';
import GlobalContext from '../../utils/GlobalContext';
import { CLASSES } from '../../graphql/Class';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';

const ClassPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [klass, setKlass] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Kelas', field: 'grade' },
    {
      title: 'Tanggal Dibuat',
      field: 'created_at',
      width: 500,
      type: 'datetime'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    setGlobalLoading(true);
    const getClasses = async () => {
      const {
        data: { classes }
      } = await query({
        query: CLASSES,
        fetchPolicy: 'no-cache'
      });
      setKlass(classes);
    };

    getClasses();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={klass}
        title="Kelas"
        addUrl="/class/new"
        setOpen={setOpen}
        setTempData={setTempData}
        editUrl="/class/edit"
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Kelas"
        mappingRows={{
          id: 'ID Kelas',
          grade: 'Nama Kelas',
          created_at: 'Tanggal Dibuat'
        }}
      />
    </div>
  );
};

export default ClassPage;
