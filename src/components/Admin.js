import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Route, Switch, Link, Redirect } from 'react-router-dom';
import { Tooltip, Collapse } from '@material-ui/core';
import AppBar from '@material-ui/core/AppBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Cookies from 'universal-cookie';
import {
  ExpandLess,
  ExpandMore,
  Input,
  Dashboard,
  Menu as MenuIcon,
  PowerSettingsNew,
} from '@material-ui/icons';

import DashboardPage from './Dashboard/DashboardPage';
import PublisherPage from './Publisher/PublisherPage';
import PublisherForm from './Publisher/PublisherForm';
import AuthorPage from './Author/AuthorPage';
import AuthorForm from './Author/AuthorForm';
import BookshelfPage from './Bookshelf/BookshelfPage';
import BookshelfForm from './Bookshelf/BookshelfForm';
import ClassPage from './Class/ClassPage';
import ClassForm from './Class/ClassForm';
import HolidayPage from './Holiday/HolidayPage';
import HolidayForm from './Holiday/HolidayForm';
import BookPage from './Book/BookPage';
import BookForm from './Book/BookForm';
import MemberPage from './Member/MemberPage';
import MemberForm from './Member/MemberForm';
import OfficerPage from './Officer/OfficerPage';
import OfficerForm from './Officer/OfficerForm';
import BorrowPage from './Borrow/BorrowPage';
import BorrowForm from './Borrow/BorrowForm';
import ReturnPage from './Return/ReturnPage';

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
  },
  appBar: {
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

const Admin = (props) => {
  const { container, query, history, mutate } = props;
  const classes = useStyles();
  const theme = useTheme();
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const [dropdownMaster, setDropdownMaster] = useState(false);
  const [dropdownMember, setDropdownMember] = useState(false);
  const [dropdownTransaction, setDropdownTransaction] = useState(false);
  const logout = () => {
    const cookie = new Cookies();
    cookie.remove('id');
    cookie.remove('name');
    cookie.remove('token', { path: '/' });
    history.replace('/login');
  };

  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };

  const drawer = (
    <div>
      <div className={classes.toolbar} />
      <Divider />
      <List>
        <Tooltip title="Dashboard">
          <ListItem component={Link} to="/dashboard" button key="Dashboard">
            <ListItemIcon>
              <Dashboard />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
        </Tooltip>
        <Tooltip title="Master">
          <ListItem button onClick={() => setDropdownMaster(!dropdownMaster)}>
            <ListItemIcon>
              <Input />
            </ListItemIcon>
            <ListItemText primary="Master" />
            {dropdownMaster ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
        </Tooltip>
        <Collapse in={dropdownMaster} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Tooltip title="Penerbit">
              <ListItem component={Link} to="/publisher" button>
                <Typography noWrap>
                  <ListItemText inset primary="Penerbit" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
          <List component="div" disablePadding>
            <Tooltip title="Pengarang">
              <ListItem component={Link} to="/author" button>
                <Typography noWrap>
                  <ListItemText inset primary="Pengarang" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
          <List component="div" disablePadding>
            <Tooltip title="Rak Buku">
              <ListItem component={Link} to="/bookshelf" button>
                <Typography noWrap>
                  <ListItemText inset primary="Rak Buku" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
          <List component="div" disablePadding>
            <Tooltip title="Kelas">
              <ListItem component={Link} to="/class" button>
                <Typography noWrap>
                  <ListItemText inset primary="Kelas" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
          <List component="div" disablePadding>
            <Tooltip title="Hari Libur">
              <ListItem component={Link} to="/holiday" button>
                <Typography noWrap>
                  <ListItemText inset primary="Hari Libur" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
          <List component="div" disablePadding>
            <Tooltip title="Buku">
              <ListItem component={Link} to="/book" button>
                <Typography noWrap>
                  <ListItemText inset primary="Buku" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
        </Collapse>
        <Tooltip title="Pengguna">
          <ListItem button onClick={() => setDropdownMember(!dropdownMember)}>
            <ListItemIcon>
              <Input />
            </ListItemIcon>
            <ListItemText primary="Pengguna" />
            {dropdownMember ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
        </Tooltip>
        <Collapse in={dropdownMember} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Tooltip title="Daftar Anggota">
              <ListItem component={Link} to="/member" button>
                <Typography noWrap>
                  <ListItemText inset primary="Daftar Anggota" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
          <List component="div" disablePadding>
            <Tooltip title="Daftar Petugas">
              <ListItem component={Link} to="/officer" button>
                <Typography noWrap>
                  <ListItemText inset primary="Daftar Petugas" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
        </Collapse>
        <Tooltip title="Transaksi">
          <ListItem
            button
            onClick={() => setDropdownTransaction(!dropdownTransaction)}
          >
            <ListItemIcon>
              <Input />
            </ListItemIcon>
            <ListItemText primary="Transaksi" />
            {dropdownTransaction ? <ExpandLess /> : <ExpandMore />}
          </ListItem>
        </Tooltip>
        <Collapse in={dropdownTransaction} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            <Tooltip title="Peminjaman Buku">
              <ListItem component={Link} to="/borrow" button>
                <Typography noWrap>
                  <ListItemText inset primary="Peminjaman Buku" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
          <List component="div" disablePadding>
            <Tooltip title="Pengembalian Buku">
              <ListItem component={Link} to="/return" button>
                <Typography noWrap>
                  <ListItemText inset primary="Pengembalian Buku" />
                </Typography>
              </ListItem>
            </Tooltip>
          </List>
        </Collapse>
        <Divider />
        <List>
          <Tooltip title="Log Out">
            <ListItem button key="Logout" onClick={logout}>
              <ListItemIcon>
                <PowerSettingsNew />
              </ListItemIcon>
              <ListItemText primary="Log Out" />
            </ListItem>
          </Tooltip>
        </List>
      </List>
      <Divider />
    </div>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap>
            Sistem Informasi Perpustakaan SMP PGRI 6 Surabaya
          </Typography>
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper,
            }}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <Switch>
          <Route
            exact
            path="/dashboard"
            render={(props) => <DashboardPage query={query} {...props} />}
          />
          <Route
            exact
            path="/publisher"
            render={(props) => <PublisherPage query={query} {...props} />}
          />
          <Route
            exact
            path="/publisher/new"
            render={(props) => (
              <PublisherForm
                PublisherForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/publisher/edit/:id"
            render={(props) => (
              <PublisherForm
                PublisherForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />

          <Route
            exact
            path="/author"
            render={(props) => <AuthorPage query={query} {...props} />}
          />
          <Route
            exact
            path="/author/new"
            render={(props) => (
              <AuthorForm
                AuthorForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/author/edit/:id"
            render={(props) => (
              <AuthorForm
                AuthorForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />

          <Route
            exact
            path="/bookshelf"
            render={(props) => <BookshelfPage query={query} {...props} />}
          />
          <Route
            exact
            path="/bookshelf/new"
            render={(props) => (
              <BookshelfForm
                BookshelfForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/bookshelf/edit/:id"
            render={(props) => (
              <BookshelfForm
                BookshelfForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />

          <Route
            exact
            path="/class"
            render={(props) => <ClassPage query={query} {...props} />}
          />
          <Route
            exact
            path="/class/new"
            render={(props) => (
              <ClassForm
                ClassForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/class/edit/:id"
            render={(props) => (
              <ClassForm
                ClassForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />

          <Route
            exact
            path="/holiday"
            render={(props) => <HolidayPage query={query} {...props} />}
          />
          <Route
            exact
            path="/holiday/new"
            render={(props) => (
              <HolidayForm
                HolidayForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/holiday/edit/:id"
            render={(props) => (
              <HolidayForm
                HolidayForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />

          <Route
            exact
            path="/book"
            render={(props) => <BookPage query={query} {...props} />}
          />
          <Route
            exact
            path="/book/new"
            render={(props) => (
              <BookForm
                BookForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/book/edit/:id"
            render={(props) => (
              <BookForm
                BookForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />
          <Route
            exact
            path="/member"
            render={(props) => (
              <MemberPage query={query} mutate={mutate} {...props} />
            )}
          />
          <Route
            exact
            path="/member/new"
            render={(props) => (
              <MemberForm
                MemberForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/member/edit/:id"
            render={(props) => (
              <MemberForm
                MemberForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />

          <Route
            exact
            path="/officer"
            render={(props) => <OfficerPage query={query} {...props} />}
          />
          <Route
            exact
            path="/officer/new"
            render={(props) => (
              <OfficerForm
                OfficerForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={true}
              />
            )}
          />
          <Route
            exact
            path="/officer/edit/:id"
            render={(props) => (
              <OfficerForm
                OfficerForm
                query={query}
                mutate={mutate}
                {...props}
                isAdd={false}
              />
            )}
          />

          <Route
            exact
            path="/borrow"
            render={(props) => (
              <BorrowPage query={query} mutate={mutate} {...props} />
            )}
          />

          <Route
            exact
            path="/borrow/new"
            render={(props) => (
              <BorrowForm query={query} mutate={mutate} {...props} />
            )}
          />

          <Route
            exact
            path="/return"
            render={(props) => (
              <ReturnPage query={query} mutate={mutate} {...props} />
            )}
          />

          <Redirect exact from="/" to="/dashboard" />
        </Switch>
      </main>
    </div>
  );
};

Admin.propTypes = {
  container: PropTypes.instanceOf(
    typeof Element === 'undefined' ? Object : Element
  ),
};

export default Admin;
