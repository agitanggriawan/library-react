import React, { useState, useEffect, useContext } from 'react';
import GlobalContext from '../../utils/GlobalContext';
import { HOLIDAYS } from '../../graphql/Holiday';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';

const HolidayPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [holiday, setHoliday] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Tanggal Libur', field: 'date_off', type: 'date' },
    { title: 'Keterangan', field: 'name' },
    {
      title: 'Tanggal Dibuat',
      field: 'created_at',
      width: 500,
      type: 'datetime'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    setGlobalLoading(true);
    const getHolidays = async () => {
      const {
        data: { holidays }
      } = await query({
        query: HOLIDAYS,
        fetchPolicy: 'no-cache'
      });
      setHoliday(holidays);
    };

    getHolidays();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={holiday}
        title="Hari Libur"
        addUrl="/holiday/new"
        setOpen={setOpen}
        setTempData={setTempData}
        editUrl="/holiday/edit"
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Hari Libur"
        mappingRows={{
          id: 'ID Hari Libur',
          date_off: 'Tanggal Libur',
          name: 'Keterangan',
          created_at: 'Tanggal Dibuat'
        }}
      />
    </div>
  );
};

export default HolidayPage;
