import React, { useContext, useEffect, useState } from 'react';
import { Grid, Paper, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import moment from 'moment-timezone';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { CREATE_HOLIDAY, HOLIDAY, UPDATE_HOLIDAY } from '../../graphql/Holiday';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const HolidayForm = props => {
  const { history, query, mutate, isAdd, match } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [holiday, setHoliday] = useState({});
  const [selectedDate, setSelectedDate] = React.useState(new Date());
  const handleDateChange = date => {
    setSelectedDate(date);
  };

  useEffect(() => {
    if (match.params.id) {
      setGlobalLoading(true);
      const getHoliday = async () => {
        const {
          data: { holiday }
        } = await query({
          query: HOLIDAY,
          variables: {
            id: match.params.id
          },
          fetchPolicy: 'no-cache'
        });
        console.log('holiday', holiday);
        setGlobalLoading(false);
        setHoliday(holiday);
        setSelectedDate(holiday.date_off);
      };
      getHoliday();
    }
  }, [match.params.id, query, setGlobalLoading]);

  const AddPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Hari Libur', url: '/holiday' }}
        tertiary={{ title: 'Tambah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              date_off: new Date(),
              name: ''
            }}
            validationSchema={Yup.object().shape({
              date_off: Yup.string().required('Harus diisi'),
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ date_off, name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { createHoliday }
                } = await mutate({
                  mutation: CREATE_HOLIDAY,
                  variables: {
                    data: {
                      date_off: moment(date_off).format('YYYY-MM-DD'),
                      name
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setSnack({
                  variant: 'success',
                  message: `${createHoliday.name} Berhasil disimpan`,
                  opened: true
                });
                setGlobalLoading(false);
                resetForm();
                history.replace('/holiday');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({
              errors,
              touched,
              handleSubmit,
              values,
              handleChange,
              setFieldValue
            }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container justify="space-around">
                          <KeyboardDatePicker
                            margin="normal"
                            id="date_off"
                            name="date_off"
                            label="Tanggal Libur"
                            format="MM/dd/yyyy"
                            value={values.date_off}
                            onChange={e => {
                              setFieldValue('birthdate', e);
                            }}
                            KeyboardButtonProps={{
                              'aria-label': 'change date'
                            }}
                          />
                        </Grid>
                      </MuiPickersUtilsProvider>
                      {errors.date_off && touched.date_off && (
                        <div style={{ color: 'red' }}>{errors.date_off}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Keterangan"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/holiday')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  const EditPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Hari Libur', url: '/holiday' }}
        tertiary={{ title: 'Ubah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              date_off: selectedDate,
              name: holiday.name
            }}
            validationSchema={Yup.object().shape({
              date_off: Yup.string().required('Harus diisi'),
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ date_off, name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { updateHoliday }
                } = await mutate({
                  mutation: UPDATE_HOLIDAY,
                  variables: {
                    id: match.params.id,
                    data: {
                      date_off: moment(date_off).format('YYYY-MM-DD'),
                      name
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setGlobalLoading(false);
                setSnack({
                  variant: 'success',
                  message: `${updateHoliday.name} Berhasil diubah`,
                  opened: true
                });
                resetForm();
                history.replace('/holiday');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container justify="space-around">
                          <KeyboardDatePicker
                            margin="normal"
                            id="date-picker-dialog"
                            label="Tanggal Libur"
                            format="MM/dd/yyyy"
                            value={selectedDate}
                            onChange={handleDateChange}
                            KeyboardButtonProps={{
                              'aria-label': 'change date'
                            }}
                          />
                        </Grid>
                      </MuiPickersUtilsProvider>
                      {errors.date_off && touched.date_off && (
                        <div style={{ color: 'red' }}>{errors.date_off}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Keterangan"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        UPDATE
                      </Button>
                      <Button
                        onClick={() => history.replace('/holiday')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  return <div>{isAdd ? <AddPage /> : <EditPage />}</div>;
};

export default HolidayForm;
