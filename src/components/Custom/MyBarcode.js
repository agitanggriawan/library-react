import React from 'react';
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  DialogActions
} from '@material-ui/core/';
import Barcode from 'react-barcode';
import ReactToPrint from 'react-to-print';

class ComponentToPrint extends React.Component {
  render() {
    const { data } = this.props;
    return <Barcode displayValue={false} value={data.uuid} />;
  }
}

const MyBarcode = props => {
  const { data, open, handleClose } = props;
  const componentRef = React.useRef();

  return (
    <>
      <Dialog
        open={open}
        fullWidth
        maxWidth={'md'}
        onClose={handleClose}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle id="draggable-dialog-title">
          Barcode {data.title}
        </DialogTitle>
        <DialogContent>
          <DialogContentText style={{ textAlign: 'center' }}>
            <ComponentToPrint data={data} ref={componentRef} />
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Tutup
          </Button>
          <ReactToPrint
            trigger={() => <Button color="primary">Cetak Barcode</Button>}
            content={() => componentRef.current}
          />
        </DialogActions>
      </Dialog>
    </>
  );
};

export default MyBarcode;
