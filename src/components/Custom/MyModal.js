import React from 'react';
import {
  Button,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  DialogActions,
  Table,
  TableCell,
  TableBody,
  TableRow
} from '@material-ui/core/';
import moment from 'moment-timezone';
import NumberFormat from 'react-number-format';
import { getSchoolDays } from '../../utils';

const MyModal = props => {
  const {
    open,
    handleClose,
    type,
    reMap,
    mappingRows,
    handleReturnBook,
    isBorrow,
    isReturn,
    isVerify,
    holiday,
    handleVerify
  } = props;
  let { data } = props;

  if (reMap) {
    const diffs = getSchoolDays(data.max_return, moment(), holiday);
    diffs.shift();

    data = {
      ...data,
      class: data.member
        ? data.member.class
          ? data.member.class.grade
          : null
        : null,
      title: data.book ? data.book.title : null,
      code_number: data.book ? data.book.code_number : null,
      city: data.book ? data.book.city : null,
      year: data.book ? data.book.year : null,
      publisher: data.book
        ? data.book.publisher
          ? data.book.publisher.name
          : null
        : null,
      author: data.book
        ? data.book.author
          ? data.book.author.name
          : null
        : null,
      late: diffs.length < 0 ? '-' : `${diffs.length} Hari`,
      penalty:
        diffs.length < 0 ? (
          0
        ) : (
          <NumberFormat
            value={diffs.length * 1000}
            thousandSeparator
            displayType="text"
            prefix="Rp. "
          />
        )
    };
  }

  if (isReturn) {
    const diffs = getSchoolDays(data.max_return, moment(), holiday);
    diffs.shift();

    data = {
      ...data,
      class: data.member
        ? data.member.class
          ? data.member.class.grade
          : null
        : null,
      title: data.book ? data.book.title : null,
      code_number: data.book ? data.book.code_number : null,
      city: data.book ? data.book.city : null,
      year: data.book ? data.book.year : null,
      publisher: data.book
        ? data.book.publisher
          ? data.book.publisher.name
          : null
        : null,
      author: data.book
        ? data.book.author
          ? data.book.author.name
          : null
        : null,
      late: diffs.length < 0 ? '-' : `${diffs.length} Hari`,
      penalty:
        diffs.length < 0 ? (
          0
        ) : (
          <NumberFormat
            value={diffs.length * 1000}
            thousandSeparator
            displayType="text"
            prefix="Rp. "
          />
        )
    };
  }

  return (
    <>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="draggable-dialog-title"
      >
        <DialogTitle id="draggable-dialog-title">Detail {type}</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Table aria-label="simple-table">
              <TableBody>
                {Object.keys(mappingRows).map((x, i) => (
                  <TableRow key={i}>
                    <TableCell style={{ fontWeight: 'bold' }}>
                      {typeof mappingRows[x] === 'object'
                        ? mappingRows[x].title
                        : mappingRows[x]}
                    </TableCell>
                    <TableCell>
                      {typeof mappingRows[x] === 'object'
                        ? data[Object.keys(mappingRows)[i]]
                          ? data[Object.keys(mappingRows)[i]][
                              mappingRows[x].row
                            ]
                          : null
                        : data[Object.keys(mappingRows)[i]]}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Tutup
          </Button>
          {reMap && isBorrow && (
            <Button
              onClick={() => handleReturnBook(data.id, data.penalty)}
              color="secondary"
              autoFocus
            >
              Kembalikan
            </Button>
          )}

          {isVerify && data.status === 'Terdaftar' && (
            <Button
              onClick={() => handleVerify(data.registration_token)}
              color="default"
              autoFocus
            >
              Verifikasi
            </Button>
          )}
        </DialogActions>
      </Dialog>
    </>
  );
};

export default MyModal;
