import React from 'react';
import MaterialTable from 'material-table';

const MyTable = (props) => {
  const {
    history,
    columns,
    data,
    title,
    addUrl,
    setOpen,
    setTempData,
    editUrl,
    isFilter,
    setOpenFilter,
    isBarcode,
    setOpenBarcode,
  } = props;

  const addButton = {
    icon: 'add',
    tooltip: `Tambah ${title}`,
    isFreeAction: true,
    onClick: (event) => {
      history.replace(addUrl);
    },
  };

  const editButton = {
    icon: 'edit',
    tooltip: `Edit ${title}`,
    onClick: (event, rowData) => {
      history.replace(`${editUrl}/${rowData.id}`);
    },
  };

  const filterButton = {
    icon: 'filter_list',
    tooltip: 'Saring Data',
    isFreeAction: true,
    onClick: (event, rowData) => {
      setOpenFilter(true);
    },
  };

  const barcodeButton = {
    icon: 'save_alt',
    tooltip: 'Lihat Barcode',
    onClick: (event, rowData) => {
      setOpenBarcode(true);
      setTempData(rowData);
    },
  };

  return (
    <div>
      <MaterialTable
        columns={columns}
        data={data}
        actions={[
          isBarcode ? { ...barcodeButton } : null,
          editUrl ? { ...editButton } : null,
          {
            icon: 'chevron_right',
            tooltip: `Detail ${title}`,
            onClick: (event, rowData) => {
              setOpen(true);
              setTempData(rowData);
            },
          },
          addUrl ? { ...addButton } : null,
          isFilter ? { ...filterButton } : null,
        ]}
        options={{
          actionsColumnIndex: -1,
          pageSize: 10,
        }}
        title={`Data ${title}`}
        localization={{
          pagination: {
            labelDisplayedRows: '{from}-{to} dari {count}',
            labelRowsSelect: 'Baris Data',
            firstAriaLabel: 'Halaman Pertama',
            firstTooltip: 'Halaman Pertama',
            previousAriaLabel: 'Halaman Sebelumnya',
            previousTooltip: 'Halaman Sebelumnya',
            nextAriaLabel: 'Halaman Selanjutnya',
            nextTooltip: 'Halaman Selanjutnya',
            lastAriaLabel: 'Halaman Terakhir',
            lastTooltip: 'Halaman Terakhir',
          },
          header: {
            actions: 'Aksi',
          },
          body: {
            emptyDataSourceMessage: 'Data tidak ditemukan',
            filterRow: {
              filterTooltip: 'Saring',
            },
          },
          toolbar: {
            searchTooltip: 'Cari',
            searchPlaceholder: 'Cari',
          },
        }}
      />
    </div>
  );
};

export default MyTable;
