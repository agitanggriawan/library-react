import React from 'react';
import ModalImage from 'react-modal-image';

const MyModalImage = props => {
  const { url } = props;

  return (
    <div style={{ maxWidth: '200px' }}>
      <ModalImage small={url} large={url} />
    </div>
  );
};

export default MyModalImage;
