import React, { useEffect } from 'react';
import { Typography, Container } from '@material-ui/core';
import { VERIFY_MEMBER } from '../../graphql/Member';

const MyVerification = props => {
  const { match, mutate } = props;
  const { uuid } = match.params;

  useEffect(() => {
    const verifyMember = async () => {
      const {
        data: { verifyMember }
      } = await mutate({
        mutation: VERIFY_MEMBER,
        variables: { registration_token: uuid },
        fetchPolicy: 'no-cache',
        onError: error => {
          console.log('==> Error execute mutation', error);
        }
      });

      return verifyMember;
    };

    verifyMember();
  }, [mutate, uuid]);

  return (
    <Container maxWidth="sm">
      <div>
        <img
          alt="logo"
          src="https://tools.atimo.us/images/thanks_email_verification.jpg"
          style={{
            display: 'block',
            margin: '15vh auto 10vh auto',
            width: '100%'
          }}
        />
        <Typography align="center" variant="subtitle2">
          Selamat, status Anggota Perpustakaan SMP PGRI 6 Surabaya Anda telah
          Aktif. Terima kasih sudah mendaftar.
        </Typography>
      </div>
    </Container>
  );
};

export default MyVerification;
