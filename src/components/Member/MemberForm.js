import React, { useContext, useEffect, useState } from 'react';
import {
  Grid,
  Paper,
  TextField,
  Button,
  InputLabel,
  Select,
  MenuItem,
  FormControl
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import ImageUploader from 'react-images-upload';
import moment from 'moment-timezone';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { CREATE_MEMBER, MEMBER, UPDATE_MEMBER } from '../../graphql/Member';
import { CLASSES } from '../../graphql/Class';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const MemberForm = props => {
  const { history, query, mutate, isAdd, match } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [member, setMember] = useState({});
  const [klass, setKlass] = useState([]);

  useEffect(() => {
    if (match.params.id) {
      setGlobalLoading(true);
      const getMember = async () => {
        const { data } = await query({
          query: MEMBER,
          variables: {
            id: match.params.id
          },
          fetchPolicy: 'no-cache'
        });

        setGlobalLoading(false);
        setMember(data.member);
      };
      getMember();
    }
  }, [match.params.id, query, setGlobalLoading]);

  useEffect(() => {
    const getClasses = async () => {
      const {
        data: { classes }
      } = await query({
        query: CLASSES,
        fetchPolicy: 'no-cache'
      });
      setKlass(classes);
    };

    getClasses();
  }, [query]);

  const AddPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Anggota', url: '/member' }}
        tertiary={{ title: 'Tambah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            initialValues={{
              class_id: '',
              name: '',
              address: '',
              gender: '',
              phone: '',
              birthdate: new Date(),
              photo: [],
              email: ''
            }}
            validationSchema={Yup.object().shape({
              class_id: Yup.string().required('Harus diisi'),
              name: Yup.string().required('Harus diisi'),
              gender: Yup.string().required('Harus diisi'),
              email: Yup.string().email('Email tidak valid')
            })}
            onSubmit={async (
              {
                class_id,
                name,
                address,
                gender,
                phone,
                birthdate,
                photo,
                email
              },
              { resetForm }
            ) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { createMember }
                } = await mutate({
                  mutation: CREATE_MEMBER,
                  variables: {
                    data: {
                      class_id,
                      name,
                      address,
                      gender,
                      phone,
                      birthdate: moment(birthdate).format('YYYY-MM-DD'),
                      photo,
                      email
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setSnack({
                  variant: 'success',
                  message: `${createMember.name} Berhasil disimpan`,
                  opened: true
                });
                setGlobalLoading(false);
                resetForm();
                history.replace('/member');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({
              errors,
              touched,
              handleSubmit,
              values,
              handleChange,
              setFieldValue
            }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={3} lg={3}>
                      <FormControl margin="normal" fullWidth>
                        <InputLabel id="class_id">Kelas</InputLabel>
                        <Select
                          autoFocus
                          margin="normal"
                          fullWidth
                          id="class_id"
                          label="Nama Anggota"
                          name="class_id"
                          onChange={handleChange}
                          value={values.class_id}
                        >
                          <MenuItem value="" disabled>
                            --- Pilih ---
                          </MenuItem>
                          {klass.length
                            ? klass.map(x => (
                                <MenuItem value={x.id}>{x.grade}</MenuItem>
                              ))
                            : null}
                        </Select>
                      </FormControl>
                      {errors.class_id && touched.class_id && (
                        <div style={{ color: 'red' }}>{errors.class_id}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={5} lg={5}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Anggota"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="email"
                        label="Email Anggota"
                        name="email"
                        onChange={handleChange}
                        value={values.email}
                      />
                      {errors.email && touched.email && (
                        <div style={{ color: 'red' }}>{errors.email}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        multiline
                        rows={5}
                        name="address"
                        fullWidth
                        id="address"
                        label="Alamat"
                        onChange={handleChange}
                        value={values.address}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={3}>
                      <FormControl margin="normal" fullWidth>
                        <InputLabel id="gender">Jenis Kelamin</InputLabel>
                        <Select
                          margin="normal"
                          fullWidth
                          id="gender"
                          label="Nama Anggota"
                          name="gender"
                          onChange={handleChange}
                          value={values.gender}
                        >
                          <MenuItem value="" disabled>
                            --- Pilih ---
                          </MenuItem>
                          <MenuItem value="Laki-laki">Laki-laki</MenuItem>
                          <MenuItem value="Perempuan">Perempuan</MenuItem>
                        </Select>
                      </FormControl>
                      {errors.gender && touched.gender && (
                        <div style={{ color: 'red' }}>{errors.gender}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={4}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container justify="space-around">
                          <KeyboardDatePicker
                            margin="normal"
                            id="birthdate"
                            name="birthdate"
                            label="Tanggal Lahir"
                            format="MM/dd/yyyy"
                            value={values.birthdate}
                            onChange={e => {
                              setFieldValue('birthdate', e);
                            }}
                            KeyboardButtonProps={{
                              'aria-label': 'change date'
                            }}
                            disableFuture
                          />
                        </Grid>
                      </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={5}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="phone"
                        label="Nomor Telepon"
                        name="phone"
                        onChange={handleChange}
                        value={values.phone}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <ImageUploader
                        withPreview
                        withIcon={true}
                        buttonText="Pilih Gambar"
                        onChange={e => {
                          setFieldValue('photo', e);
                        }}
                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                        maxFileSize={5242880}
                      />
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/member')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  const EditPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Anggota', url: '/member' }}
        tertiary={{ title: 'Ubah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              class_id: member.class_id,
              name: member.name,
              address: member.address,
              gender: member.gender,
              phone: member.phone,
              birthdate: member.birthdate,
              photo: member.photo
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async (
              { class_id, name, address, gender, phone, birthdate, photo },
              { resetForm }
            ) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { updateMember }
                } = await mutate({
                  mutation: UPDATE_MEMBER,
                  variables: {
                    id: match.params.id,
                    data: {
                      class_id,
                      name,
                      address,
                      gender,
                      phone,
                      birthdate: moment(birthdate).format('YYYY-MM-DD'),
                      photo
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setGlobalLoading(false);
                setSnack({
                  variant: 'success',
                  message: `${updateMember.name} Berhasil diubah`,
                  opened: true
                });
                resetForm();
                history.replace('/member');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({
              errors,
              touched,
              handleSubmit,
              values,
              handleChange,
              setFieldValue
            }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={6} lg={3}>
                      <FormControl margin="normal" fullWidth>
                        <InputLabel id="class_id">Kelas</InputLabel>
                        <Select
                          autoFocus
                          margin="normal"
                          fullWidth
                          id="class_id"
                          label="Nama Anggota"
                          name="class_id"
                          onChange={handleChange}
                          value={values.class_id}
                        >
                          <MenuItem value="" disabled>
                            --- Pilih ---
                          </MenuItem>
                          {klass.length
                            ? klass.map(x => (
                                <MenuItem value={x.id}>
                                  Kelas {x.grade}
                                </MenuItem>
                              ))
                            : null}
                        </Select>
                      </FormControl>
                      {errors.class_id && touched.class_id && (
                        <div style={{ color: 'red' }}>{errors.class_id}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={9}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Anggota"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        multiline
                        rows={5}
                        name="address"
                        fullWidth
                        id="address"
                        label="Alamat"
                        onChange={handleChange}
                        value={values.address}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={3}>
                      <FormControl margin="normal" fullWidth>
                        <InputLabel id="gender">Jenis Kelamin</InputLabel>
                        <Select
                          margin="normal"
                          fullWidth
                          id="gender"
                          label="Nama Anggota"
                          name="gender"
                          onChange={handleChange}
                          value={values.gender}
                        >
                          <MenuItem value="" disabled>
                            --- Pilih ---
                          </MenuItem>
                          <MenuItem value="Laki-laki">Laki-laki</MenuItem>
                          <MenuItem value="Perempuan">Perempuan</MenuItem>
                        </Select>
                      </FormControl>
                      {errors.gender && touched.gender && (
                        <div style={{ color: 'red' }}>{errors.gender}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={4}>
                      <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <Grid container justify="space-around">
                          <KeyboardDatePicker
                            margin="normal"
                            id="birthdate"
                            name="birthdate"
                            label="Tanggal Lahir"
                            format="MM/dd/yyyy"
                            value={values.birthdate}
                            onChange={e => {
                              setFieldValue('birthdate', e);
                            }}
                            KeyboardButtonProps={{
                              'aria-label': 'change date'
                            }}
                            disableFuture
                          />
                        </Grid>
                      </MuiPickersUtilsProvider>
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={5}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="phone"
                        label="Nomor Telepon"
                        name="phone"
                        onChange={handleChange}
                        value={values.phone}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <ImageUploader
                        withPreview
                        withIcon={true}
                        buttonText="Pilih Gambar"
                        onChange={e => {
                          setFieldValue('photo', e);
                        }}
                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                        maxFileSize={5242880}
                      />
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/member')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  return <div>{isAdd ? <AddPage /> : <EditPage />}</div>;
};

export default MemberForm;
