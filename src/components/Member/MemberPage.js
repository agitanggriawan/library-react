import React, { useState, useEffect, useContext } from 'react';
import { Chip } from '@material-ui/core';
import GlobalContext from '../../utils/GlobalContext';
import { MEMBERS, VERIFY_MEMBER } from '../../graphql/Member';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';
import MyModalImage from '../Custom/MyModalImage';

const MemberPage = (props) => {
  const { query, history, mutate } = props;
  const { setGlobalLoading, setSnack } = useContext(GlobalContext);
  const [member, setMember] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: (rowData) => `${rowData.tableData.id + 1}.`,
      width: 100,
    },
    { title: 'Nama Lengkap', field: 'name' },
    { title: 'Kelas', field: 'class.grade', defaultSort: 'asc' },
    { title: 'Alamat', field: 'address' },
    { title: 'Jenis Kelamin', field: 'gender' },
    {
      title: 'Status',
      render: (rowData) => {
        return <Chip label={rowData.status} color="primary" />;
      },
    },
    {
      title: 'Foto',
      render: (rowData) => {
        if (rowData.photo) {
          return (
            <div>
              <MyModalImage
                url={`http://localhost:5000/public/${rowData.photo}`}
              />
            </div>
          );
        }
      },
    },
  ];

  const handleClose = () => {
    setOpen(false);
  };

  const handleVerify = async (registration_token) => {
    setGlobalLoading(true);
    setOpen(false);
    const {
      data: { verifyMember },
    } = await mutate({
      mutation: VERIFY_MEMBER,
      variables: { registration_token },
      fetchPolicy: 'no-cache',
      onError: (error) => {
        console.log('==> Error execute mutation', error);
      },
    });

    setSnack({
      variant: 'success',
      message: 'Verifikasi anggota berhasil',
      opened: true,
    });

    setGlobalLoading(false);
    window.location.reload();
    return verifyMember;
  };

  useEffect(() => {
    setGlobalLoading(true);
    const getPublishers = async () => {
      const {
        data: { members },
      } = await query({
        query: MEMBERS,
        fetchPolicy: 'no-cache',
      });
      setMember(members);
    };

    getPublishers();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={member}
        title="Anggota"
        addUrl="/member/new"
        setOpen={setOpen}
        setTempData={setTempData}
        editUrl="/member/edit"
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        handleVerify={handleVerify}
        data={tempData}
        type="Anggota"
        isVerify={true}
        mappingRows={{
          id: 'ID Anggota',
          name: 'Nama Lengkap',
          email: 'Email',
          class: {
            title: 'Kelas',
            row: 'grade',
          },
          address: 'Alamat',
          gender: 'Jenis Kelamin',
          phone: 'Nomor Telepon',
          birthdate: 'Tanggal Lahir',
          created_at: 'Tanggal Dibuat',
        }}
      />
    </div>
  );
};

export default MemberPage;
