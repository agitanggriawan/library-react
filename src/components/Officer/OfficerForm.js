import React, { useContext, useEffect, useState } from 'react';
import { Grid, Paper, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { CREATE_OFFICER, OFFICER, UPDATE_OFFICER } from '../../graphql/Officer';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const OfficerForm = props => {
  const { history, query, mutate, isAdd, match } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [officer, setOfficer] = useState({});

  useEffect(() => {
    if (match.params.id) {
      setGlobalLoading(true);
      const getOfficer = async () => {
        const {
          data: { officer }
        } = await query({
          query: OFFICER,
          variables: {
            id: match.params.id
          },
          fetchPolicy: 'no-cache'
        });

        setGlobalLoading(false);
        setOfficer(officer);
      };
      getOfficer();
    }
  }, [match.params.id, query, setGlobalLoading]);

  const AddPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Petugas', url: '/officer' }}
        tertiary={{ title: 'Tambah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            initialValues={{
              name: '',
              email: '',
              address: '',
              phone: '',
              password: '',
              passwordConfirmation: ''
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi'),
              email: Yup.string()
                .required('Harus diisi')
                .email('Format email tidak valid'),
              address: Yup.string().required('Harus diisi'),
              phone: Yup.string().required('Harus diisi'),
              password: Yup.string().required('Harus diisi'),
              passwordConfirmation: Yup.string().oneOf(
                [Yup.ref('password'), null],
                'Password harus sama'
              )
            })}
            onSubmit={async (
              { name, email, address, phone, password },
              { resetForm }
            ) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { createOfficer }
                } = await mutate({
                  mutation: CREATE_OFFICER,
                  variables: {
                    data: {
                      name,
                      role: 'Admin',
                      email,
                      address,
                      phone,
                      password
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setSnack({
                  variant: 'success',
                  message: `${createOfficer.name} Berhasil disimpan`,
                  opened: true
                });
                setGlobalLoading(false);
                resetForm();
                history.replace('/officer');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Petugas"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="email"
                        label="Email"
                        name="email"
                        onChange={handleChange}
                        value={values.email}
                      />
                      {errors.email && touched.email && (
                        <div style={{ color: 'red' }}>{errors.email}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="phone"
                        label="Nomor Telepon"
                        name="phone"
                        onChange={handleChange}
                        value={values.phone}
                      />
                      {errors.phone && touched.phone && (
                        <div style={{ color: 'red' }}>{errors.phone}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        multiline
                        rows={5}
                        name="address"
                        fullWidth
                        id="address"
                        label="Alamat"
                        onChange={handleChange}
                        value={values.address}
                      />
                      {errors.address && touched.address && (
                        <div style={{ color: 'red' }}>{errors.address}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="password"
                        label="Kata Sandi"
                        type="password"
                        name="password"
                        onChange={handleChange}
                        value={values.password}
                      />
                      {errors.password && touched.password && (
                        <div style={{ color: 'red' }}>{errors.password}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="passwordConfirmation"
                        label="Kata Sandi"
                        type="password"
                        name="passwordConfirmation"
                        onChange={handleChange}
                        value={values.passwordConfirmation}
                      />
                      {errors.passwordConfirmation &&
                        touched.passwordConfirmation && (
                          <div style={{ color: 'red' }}>
                            {errors.passwordConfirmation}
                          </div>
                        )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/officer')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  const EditPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Petugas', url: '/officer' }}
        tertiary={{ title: 'Ubah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              name: officer.name
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { updateOfficer }
                } = await mutate({
                  mutation: UPDATE_OFFICER,
                  variables: {
                    id: match.params.id,
                    data: {
                      name
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setGlobalLoading(false);
                setSnack({
                  variant: 'success',
                  message: `${updateOfficer.name} Berhasil diubah`,
                  opened: true
                });
                resetForm();
                history.replace('/officer');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Petugas"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        UPDATE
                      </Button>
                      <Button
                        onClick={() => history.replace('/officer')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  return <div>{isAdd ? <AddPage /> : <EditPage />}</div>;
};

export default OfficerForm;
