import React, { useState, useEffect, useContext } from 'react';
import GlobalContext from '../../utils/GlobalContext';
import { OFFICERS } from '../../graphql/Officer';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';

const OfficerPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [officer, setOfficer] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Nama', field: 'name' },
    { title: 'Peran', field: 'role' },
    { title: 'Email', field: 'email' },
    { title: 'Alamat', field: 'address' },
    { title: 'Nomor Telepon', field: 'phone' },
    {
      title: 'Tanggal Dibuat',
      field: 'created_at',
      type: 'datetime'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    setGlobalLoading(true);
    const getOfficers = async () => {
      const {
        data: { officers }
      } = await query({
        query: OFFICERS,
        fetchPolicy: 'no-cache'
      });
      console.log('officers', officers);
      setOfficer(officers);
    };

    getOfficers();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={officer}
        title="Petugas"
        addUrl="/officer/new"
        setOpen={setOpen}
        setTempData={setTempData}
        editUrl="/officer/edit"
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Petugas"
        mappingRows={{
          id: 'ID Petugas',
          name: 'Nama Petugas',
          role: 'Peran',
          email: 'Email',
          address: 'Alamat',
          phone: 'Nomor Telepon',
          created_at: 'Tanggal Dibuat'
        }}
      />
    </div>
  );
};

export default OfficerPage;
