import React, { useContext, useEffect, useState } from 'react';
import { Grid, Paper, TextField, Button } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import * as Yup from 'yup';
import { Formik } from 'formik';
import { CREATE_AUTHOR, AUTHOR, UPDATE_AUTHOR } from '../../graphql/Author';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const AuthorForm = props => {
  const { history, query, mutate, isAdd, match } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [author, setAuthor] = useState({});

  useEffect(() => {
    if (match.params.id) {
      setGlobalLoading(true);
      const getAuthor = async () => {
        const {
          data: { author }
        } = await query({
          query: AUTHOR,
          variables: {
            id: match.params.id
          },
          fetchPolicy: 'no-cache'
        });
        console.log('author', author);
        setGlobalLoading(false);
        setAuthor(author);
      };
      getAuthor();
    }
  }, [match.params.id, query, setGlobalLoading]);

  const AddPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Pengarang', url: '/author' }}
        tertiary={{ title: 'Tambah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            initialValues={{
              name: ''
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { createAuthor }
                } = await mutate({
                  mutation: CREATE_AUTHOR,
                  variables: { data: { name } },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setSnack({
                  variant: 'success',
                  message: `${createAuthor.name} Berhasil disimpan`,
                  opened: true
                });
                setGlobalLoading(false);
                resetForm();
                history.replace('/author');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Pengarang"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/author')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  const EditPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Pengarang', url: '/author' }}
        tertiary={{ title: 'Ubah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              name: author.name
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { updateAuthor }
                } = await mutate({
                  mutation: UPDATE_AUTHOR,
                  variables: {
                    id: match.params.id,
                    data: {
                      name
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setGlobalLoading(false);
                setSnack({
                  variant: 'success',
                  message: `${updateAuthor.name} Berhasil diubah`,
                  opened: true
                });
                resetForm();
                history.replace('/author');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Pengarang"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        UPDATE
                      </Button>
                      <Button
                        onClick={() => history.replace('/author')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  return <div>{isAdd ? <AddPage /> : <EditPage />}</div>;
};

export default AuthorForm;
