import React, { useState, useEffect, useContext } from 'react';
import GlobalContext from '../../utils/GlobalContext';
import { AUTHORS } from '../../graphql/Author';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';

const AuthorPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [author, setAuthor] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Nama', field: 'name' },
    {
      title: 'Tanggal Dibuat',
      field: 'created_at',
      width: 500,
      type: 'datetime'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    setGlobalLoading(true);
    const getPublishers = async () => {
      const {
        data: { authors }
      } = await query({
        query: AUTHORS,
        fetchPolicy: 'no-cache'
      });
      setAuthor(authors);
    };

    getPublishers();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={author}
        title="Pengarang"
        addUrl="/author/new"
        setOpen={setOpen}
        setTempData={setTempData}
        editUrl="/author/edit"
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Pengarang"
        mappingRows={{
          id: 'ID Pengarang',
          name: 'Nama Pengarang',
          created_at: 'Tanggal Dibuat'
        }}
      />
    </div>
  );
};

export default AuthorPage;
