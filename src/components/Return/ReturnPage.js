import React, { useState, useEffect, useContext } from 'react';
import { Menu, MenuItem } from '@material-ui/core';
import GlobalContext from '../../utils/GlobalContext';
import { RETURNS } from '../../graphql/Borrow';
import { HOLIDAYS } from '../../graphql/Holiday';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';

const ReturnPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [returns, setReturn] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = useState(false);
  const [openFilter, setOpenFilter] = useState(false);
  const [holiday, setHoliday] = useState([]);

  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Nama Anggota', field: 'member.name' },
    { title: 'Kelas', field: 'member.class.grade' },
    { title: 'No. Registrasi', field: 'book.registration_number' },
    { title: 'Judul Buku', field: 'book.title' },
    {
      title: 'Tanggal Pinjam',
      field: 'borrow_at',
      type: 'date'
    },
    {
      title: 'Tanggal Kembali',
      field: 'return_at',
      type: 'date'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(() => {
    setGlobalLoading(true);
    const getReturns = async () => {
      const {
        data: { returns }
      } = await query({
        query: RETURNS,
        fetchPolicy: 'no-cache'
      });
      setReturn(returns);
    };

    getReturns();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  useEffect(() => {
    const getHolidays = async () => {
      const {
        data: { holidays }
      } = await query({
        query: HOLIDAYS,
        fetchPolicy: 'no-cache'
      });
      if (holidays && holidays.length) {
        const data = holidays.map(x => x.date_off);
        setHoliday(data);
      }
    };

    getHolidays();
  }, [query]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={returns}
        title="Pengembalian"
        setOpen={setOpen}
        setTempData={setTempData}
        isFilter={true}
        openFilter={openFilter}
        setOpenFilter={setOpenFilter}
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Pengembalian"
        isReturn={true}
        holiday={holiday}
        mappingRows={{
          id: 'ID Peminjaman',
          member: {
            title: 'Nama Anggota',
            row: 'name'
          },
          class: 'Kelas',
          book: {
            title: 'No. Registrasi',
            row: 'registration_number'
          },
          title: 'Judul Buku',
          code_number: 'Kode Buku',
          city: 'Kota',
          year: 'Tahun',
          publisher: 'Penerbit',
          author: 'Pengarang',
          late: 'Terlambat',
          penalty: 'Denda',
          borrow_at: 'Tanggal Pinjam',
          max_return: 'Batas Kembali',
          return_at: 'Tanggal Kembali'
        }}
      />

      <Menu
        id="simple-menu"
        keepMounted
        open={openFilter}
        onClose={() => setOpenFilter(false)}
      >
        <MenuItem>Bulan Ini</MenuItem>
        <MenuItem>7 Hari</MenuItem>
        <MenuItem>Semua</MenuItem>
      </Menu>
    </div>
  );
};

export default ReturnPage;
