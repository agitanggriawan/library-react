import React, { useState, useEffect, useContext } from 'react';
import GlobalContext from '../../utils/GlobalContext';
import { PUBLISHERS } from '../../graphql/Publisher';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';

const PublisherPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [publisher, setPublisher] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = React.useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Nama', field: 'name' },
    {
      title: 'Tanggal Dibuat',
      field: 'created_at',
      width: 500,
      type: 'datetime'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };
  useEffect(() => {
    setGlobalLoading(true);
    const getPublishers = async () => {
      const {
        data: { publishers }
      } = await query({
        query: PUBLISHERS,
        fetchPolicy: 'no-cache'
      });
      setPublisher(publishers);
    };

    getPublishers();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={publisher}
        title="Penerbit"
        addUrl="/publisher/new"
        setOpen={setOpen}
        setTempData={setTempData}
        editUrl="/publisher/edit"
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Penerbit"
        mappingRows={{
          id: 'ID Penerbit',
          name: 'Nama Penerbit',
          created_at: 'Tanggal Dibuat'
        }}
      />
    </div>
  );
};

export default PublisherPage;
