import React, { useContext, useEffect, useState } from 'react';
import { Grid, Paper, TextField, Button, InputLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import * as Yup from 'yup';
import { Formik } from 'formik';
import AsyncSelect from 'react-select/async';
import ImageUploader from 'react-images-upload';
import { CREATE_BOOK, BOOK, UPDATE_BOOK } from '../../graphql/Book';
import { SEARCH_PUBLISHER } from '../../graphql/Publisher';
import { SEARCH_AUTHOR } from '../../graphql/Author';
import { SEARCH_BOOKSHELF } from '../../graphql/Bookshelf';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const BookForm = props => {
  const { history, query, mutate, isAdd, match } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [book, setBook] = useState({});

  useEffect(() => {
    if (match.params.id) {
      setGlobalLoading(true);
      const getBook = async () => {
        const {
          data: { book }
        } = await query({
          query: BOOK,
          variables: {
            id: match.params.id
          },
          fetchPolicy: 'no-cache'
        });
        console.log('book', book);
        setGlobalLoading(false);
        setBook(book);
      };
      getBook();
    }
  }, [match.params.id, query, setGlobalLoading]);

  const getPublishers = async ({ name }) => {
    const {
      data: { searchPublisher }
    } = await query({
      query: SEARCH_PUBLISHER,
      variables: { name },
      fetctPolicy: 'no-cache'
    });

    const options = searchPublisher.map(l => ({
      value: l.id,
      label: l.name
    }));

    return options;
  };

  const getAuthors = async ({ name }) => {
    const {
      data: { searchAuthor }
    } = await query({
      query: SEARCH_AUTHOR,
      variables: { name },
      fetctPolicy: 'no-cache'
    });

    const options = searchAuthor.map(l => ({
      value: l.id,
      label: l.name
    }));

    return options;
  };

  const getBookshelf = async ({ name }) => {
    const {
      data: { searchBookshelf }
    } = await query({
      query: SEARCH_BOOKSHELF,
      variables: { name },
      fetctPolicy: 'no-cache'
    });

    const options = searchBookshelf.map(l => ({
      value: l.id,
      label: l.name
    }));

    return options;
  };

  const loadOptions = async (inputValue, type) => {
    if (type === 'publisher') {
      return getPublishers({
        name: inputValue
      });
    }

    if (type === 'author') {
      return getAuthors({
        name: inputValue
      });
    }

    if (type === 'bookshelf') {
      return getBookshelf({
        name: inputValue
      });
    }

    return null;
  };

  const handleInputChange = (newValue, type) => {
    if (type === 'publisher') {
      const inputValue = newValue.replace(/\W/g, '');
      return inputValue;
    }

    if (type === 'author') {
      const inputValue = newValue.replace(/\W/g, '');
      return inputValue;
    }

    if (type === 'bookshelf') {
      const inputValue = newValue.replace(/\W/g, '');
      return inputValue;
    }

    return null;
  };

  const AddPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Buku', url: '/book' }}
        tertiary={{ title: 'Tambah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            initialValues={{
              publisher: '',
              author: '',
              bookshelf: '',
              registration_number: '',
              code_number: '',
              title: '',
              quantity: '',
              city: '',
              year: '',
              cover_url: []
            }}
            validationSchema={Yup.object().shape({
              publisher: Yup.string().required('Harus diisi'),
              author: Yup.string().required('Harus diisi'),
              bookshelf: Yup.string().required('Harus diisi'),
              title: Yup.string().required('Harus diisi'),
              registration_number: Yup.string().required('Harus diisi'),
              code_number: Yup.string().required('Harus diisi'),
              quantity: Yup.string().required('Harus diisi'),
              city: Yup.string().required('Harus diisi'),
              year: Yup.string().required('Harus diisi')
            })}
            onSubmit={async (
              {
                publisher,
                author,
                bookshelf,
                title,
                registration_number,
                code_number,
                quantity,
                city,
                year,
                cover_url
              },
              { resetForm }
            ) => {
              try {
                // setGlobalLoading(true);
                const {
                  data: { createBook }
                } = await mutate({
                  mutation: CREATE_BOOK,
                  variables: {
                    data: {
                      publisher_id: publisher.value,
                      author_id: author.value,
                      bookshelf_id: bookshelf.value,
                      title,
                      registration_number,
                      code_number,
                      quantity,
                      city,
                      year,
                      cover_url
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });
                setSnack({
                  variant: 'success',
                  message: `${createBook.title} Berhasil disimpan`,
                  opened: true
                });
                setGlobalLoading(false);
                resetForm();
                history.replace('/book');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({
              errors,
              touched,
              handleSubmit,
              values,
              handleChange,
              setFieldValue
            }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <InputLabel shrink className={classes.labelAsyncSelect}>
                        Nama Penerbit *
                      </InputLabel>

                      <AsyncSelect
                        id="publisher"
                        margin="normal"
                        name="publisher"
                        placeholder="Nama Penerbit"
                        loadOptions={e => loadOptions(e, 'publisher')}
                        defaultOptions
                        values={values.publisher}
                        autoFocus
                        onInputChange={e => handleInputChange(e, 'publisher')}
                        onChange={e => {
                          setFieldValue('publisher', e);
                        }}
                      />
                      {errors.publisher && touched.publisher && (
                        <div style={{ color: 'red' }}>{errors.publisher}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <InputLabel shrink className={classes.labelAsyncSelect}>
                        Nama Pengarang *
                      </InputLabel>

                      <AsyncSelect
                        id="author"
                        margin="normal"
                        name="author"
                        placeholder="Nama Pengarang"
                        loadOptions={e => loadOptions(e, 'author')}
                        defaultOptions
                        onInputChange={e => handleInputChange(e, 'author')}
                        onChange={e => {
                          setFieldValue('author', e);
                        }}
                      />
                      {errors.author && touched.author && (
                        <div style={{ color: 'red' }}>{errors.author}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <InputLabel shrink className={classes.labelAsyncSelect}>
                        Rak Buku *
                      </InputLabel>

                      <AsyncSelect
                        id="bookshelf"
                        margin="normal"
                        name="bookshelf"
                        placeholder="Rak Buku"
                        loadOptions={e => loadOptions(e, 'bookshelf')}
                        defaultOptions
                        onInputChange={e => handleInputChange(e, 'bookshelf')}
                        onChange={e => {
                          setFieldValue('bookshelf', e);
                        }}
                      />
                      {errors.bookshelf && touched.bookshelf && (
                        <div style={{ color: 'red' }}>{errors.bookshelf}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="title"
                        label="Judul Buku"
                        name="title"
                        onChange={handleChange}
                        value={values.title}
                      />
                      {errors.title && touched.title && (
                        <div style={{ color: 'red' }}>{errors.title}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="registration_number"
                        label="Nomor Registrasi"
                        name="registration_number"
                        onChange={handleChange}
                        value={values.registration_number}
                      />
                      {errors.registration_number &&
                        touched.registration_number && (
                          <div style={{ color: 'red' }}>
                            {errors.registration_number}
                          </div>
                        )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="code_number"
                        label="Kode Buku"
                        name="code_number"
                        onChange={handleChange}
                        value={values.code_number}
                      />
                      {errors.code_number && touched.code_number && (
                        <div style={{ color: 'red' }}>{errors.code_number}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="quantity"
                        label="Jumlah"
                        name="quantity"
                        type="number"
                        inputProps={{ min: '0' }}
                        onChange={handleChange}
                        value={values.quantity}
                      />
                      {errors.quantity && touched.quantity && (
                        <div style={{ color: 'red' }}>{errors.quantity}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="city"
                        label="Kota"
                        name="city"
                        onChange={handleChange}
                        value={values.city}
                      />
                      {errors.city && touched.city && (
                        <div style={{ color: 'red' }}>{errors.city}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="year"
                        label="Tahun"
                        name="year"
                        type="number"
                        inputProps={{ min: '0' }}
                        onChange={handleChange}
                        value={values.year}
                      />
                      {errors.year && touched.year && (
                        <div style={{ color: 'red' }}>{errors.year}</div>
                      )}
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <ImageUploader
                        withPreview
                        withIcon={true}
                        buttonText="Pilih Gambar"
                        onChange={e => {
                          setFieldValue('cover_url', e);
                        }}
                        imgExtension={['.jpg', '.gif', '.png', '.gif']}
                        maxFileSize={5242880}
                      />
                    </Grid>

                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/book')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  const EditPage = () => (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Pengarang', url: '/book' }}
        tertiary={{ title: 'Ubah' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            enableReinitialize
            initialValues={{
              name: book.name
            }}
            validationSchema={Yup.object().shape({
              name: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ name }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { updateBook }
                } = await mutate({
                  mutation: UPDATE_BOOK,
                  variables: {
                    id: match.params.id,
                    data: {
                      name
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });

                setGlobalLoading(false);
                setSnack({
                  variant: 'success',
                  message: `${updateBook.name} Berhasil diubah`,
                  opened: true
                });
                resetForm();
                history.replace('/book');
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({ errors, touched, handleSubmit, values, handleChange }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="name"
                        label="Nama Pengarang"
                        name="name"
                        onChange={handleChange}
                        value={values.name}
                      />
                      {errors.name && touched.name && (
                        <div style={{ color: 'red' }}>{errors.name}</div>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        UPDATE
                      </Button>
                      <Button
                        onClick={() => history.replace('/book')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
    </div>
  );

  return <div>{isAdd ? <AddPage /> : <EditPage />}</div>;
};

export default BookForm;
