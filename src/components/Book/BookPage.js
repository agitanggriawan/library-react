import React, { useState, useEffect, useContext } from 'react';
import GlobalContext from '../../utils/GlobalContext';
import { BOOKS } from '../../graphql/Book';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';
import MyModalImage from '../Custom/MyModalImage';
import MyBarcode from '../Custom/MyBarcode';

const BookPage = props => {
  const { query, history } = props;
  const { setGlobalLoading } = useContext(GlobalContext);
  const [book, setBook] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = useState(false);
  const [openBarcode, setOpenBarcode] = useState(false);
  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`
    },
    { title: 'Judul', field: 'title' },
    { title: 'Penerbit', field: 'publisher.name' },
    { title: 'Pengarang', field: 'author.name' },
    { title: 'Rak Buku', field: 'bookshelf.name' },
    { title: 'Nomor Registrasi', field: 'registration_number' },
    { title: 'Kode Buku', field: 'code_number' },
    { title: 'Jumlah', field: 'quantity' },
    { title: 'Kota', field: 'city' },
    { title: 'Tahun', field: 'year' },
    {
      title: 'Sampul',
      render: rowData => {
        if (rowData.cover_url) {
          return (
            <div>
              <MyModalImage
                url={`http://localhost:5000/public/${rowData.cover_url}`}
              />
            </div>
          );
        }
      }
    },
    {
      title: 'Tanggal Dibuat',
      field: 'created_at',
      type: 'datetime'
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };

  const handleCloseBarcode = () => {
    setOpenBarcode(false);
  };

  useEffect(() => {
    setGlobalLoading(true);
    const getPublishers = async () => {
      const {
        data: { books }
      } = await query({
        query: BOOKS,
        fetchPolicy: 'no-cache'
      });
      setBook(books);
    };

    getPublishers();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  return (
    <div>
      {/* <img src="http://localhost:4000/public/1583077719356-db.png" /> */}
      <MyTable
        history={history}
        columns={columns}
        data={book}
        title="Buku"
        addUrl="/book/new"
        setOpen={setOpen}
        setOpenBarcode={setOpenBarcode}
        setTempData={setTempData}
        editUrl="/book/edit"
        isBarcode={true}
      />
      <MyBarcode
        open={openBarcode}
        handleClose={handleCloseBarcode}
        data={tempData}
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        data={tempData}
        type="Buku"
        mappingRows={{
          id: 'ID Buku',
          title: 'Judul Buku',
          publisher: {
            title: 'Penerbit',
            row: 'name'
          },
          author: {
            title: 'Pengarang',
            row: 'name'
          },
          bookshelf: {
            title: 'Rak Buku',
            row: 'name'
          },
          registration_number: 'Nomor Registrasi',
          code_number: 'Kode Buku',
          quantity: 'Jumlah',
          city: 'Kota',
          year: 'Tahun',
          created_at: 'Tanggal Dibuat'
        }}
      />
    </div>
  );
};

export default BookPage;
