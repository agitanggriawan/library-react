import React, { useState, useEffect, useContext } from 'react';
import Chip from '@material-ui/core/Chip';
import GlobalContext from '../../utils/GlobalContext';
import { BORROWS, RETURN_BOOK } from '../../graphql/Borrow';
import { HOLIDAYS } from '../../graphql/Holiday';
import MyModal from '../Custom/MyModal';
import MyTable from '../Custom/MyTable';
import moment from 'moment-timezone';

const BorrowPage = props => {
  const { query, history, mutate } = props;
  const { setGlobalLoading, setSnack } = useContext(GlobalContext);
  const [borrows, setBorrows] = useState([]);
  const [tempData, setTempData] = useState({});
  const [open, setOpen] = useState(false);
  const [holiday, setHoliday] = useState([]);

  const columns = [
    {
      title: 'Nomor',
      render: rowData => `${rowData.tableData.id + 1}.`,
      width: 100
    },
    { title: 'Nama Anggota', field: 'member.name' },
    { title: 'Kelas', field: 'member.class.grade' },
    { title: 'No. Registrasi', field: 'book.registration_number' },
    { title: 'Judul Buku', field: 'book.title' },
    {
      title: 'Tanggal Pinjam',
      field: 'borrow_at',
      type: 'date'
    },
    {
      title: 'Batas Kembali',
      field: 'max_return',
      type: 'date'
    },
    {
      title: 'Status',
      render: rowData => {
        if (moment() > moment(rowData.max_return)) {
          return <Chip label="Terlambat" color="secondary" />;
        }

        return <Chip label="Pinjam" color="primary" />;
      }
    }
  ];

  const handleClose = () => {
    setOpen(false);
  };

  const handleReturnBook = async (id, penalty) => {
    debugger;
    setGlobalLoading(true);
    setOpen(false);
    const {
      data: { returnBook }
    } = await mutate({
      mutation: RETURN_BOOK,
      variables: { id, penalty: penalty.props.value },
      fetchPolicy: 'no-cache',
      onError: error => {
        console.log('==> Error execute mutation', error);
      }
    });

    setSnack({
      variant: 'success',
      message: 'Pengembalian buku berhasil',
      opened: true
    });

    setGlobalLoading(false);
    window.location.reload();
    return returnBook;
  };

  useEffect(() => {
    setGlobalLoading(true);
    const getBorrows = async () => {
      const {
        data: { borrows }
      } = await query({
        query: BORROWS,
        fetchPolicy: 'no-cache'
      });
      setBorrows(borrows);
    };

    getBorrows();
    setGlobalLoading(false);
  }, [query, setGlobalLoading]);

  useEffect(() => {
    const getHolidays = async () => {
      const {
        data: { holidays }
      } = await query({
        query: HOLIDAYS,
        fetchPolicy: 'no-cache'
      });
      if (holidays && holidays.length) {
        const data = holidays.map(x => x.date_off);
        setHoliday(data);
      }
    };

    getHolidays();
  }, [query]);

  return (
    <div>
      <MyTable
        history={history}
        columns={columns}
        data={borrows}
        title="Peminjaman"
        addUrl="/borrow/new"
        setOpen={setOpen}
        setTempData={setTempData}
      />
      <MyModal
        open={open}
        handleClose={handleClose}
        handleReturnBook={handleReturnBook}
        data={tempData}
        type="Peminjaman"
        reMap={true}
        isBorrow={true}
        holiday={holiday}
        mappingRows={{
          id: 'ID Peminjaman',
          member: {
            title: 'Nama Anggota',
            row: 'name'
          },
          class: 'Kelas',
          book: {
            title: 'No. Registrasi',
            row: 'registration_number'
          },
          title: 'Judul Buku',
          code_number: 'Kode Buku',
          city: 'Kota',
          year: 'Tahun',
          publisher: 'Penerbit',
          author: 'Pengarang',
          late: 'Terlambat',
          penalty: 'Denda',
          borrow_at: 'Tanggal Pinjam',
          max_return: 'Batas Kembali'
        }}
      />
    </div>
  );
};

export default BorrowPage;
