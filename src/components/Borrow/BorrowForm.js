import React, { useContext, useState } from 'react';
import {
  Grid,
  Paper,
  TextField,
  Button,
  InputLabel,
  Tooltip,
  IconButton
} from '@material-ui/core';
import InfoIcon from '@material-ui/icons/Info';
import { makeStyles } from '@material-ui/core/styles';
import * as Yup from 'yup';
import { Formik } from 'formik';
import AsyncSelect from 'react-select/async';
import Cookies from 'universal-cookie';
import moment from 'moment-timezone';
import MyBreadcrumbs from '../Custom/MyBreadcrumbs';
import MyModal from '../Custom/MyModal';
import { CREATE_BORROW } from '../../graphql/Borrow';
import { SEARCH_BOOK, BOOK } from '../../graphql/Book';
import { SEARCH_MEMBER } from '../../graphql/Member';
import GlobalContext from '../../utils/GlobalContext';
import { setError } from '../../utils';

const usetStyles = makeStyles(theme => ({
  root: {
    padding: theme.spacing(3, 2)
  }
}));

const ClassForm = props => {
  const { history, query, mutate } = props;
  const classes = usetStyles();
  const { setSnack, setGlobalLoading } = useContext(GlobalContext);
  const [open, setOpen] = React.useState(false);
  const [book, setBook] = useState({});
  const cookie = new Cookies();

  const getBooks = async ({ title }) => {
    const {
      data: { searchBook }
    } = await query({
      query: SEARCH_BOOK,
      variables: { title },
      fetctPolicy: 'no-cache'
    });

    const options = searchBook.map(l => ({
      value: l.id,
      label: `${l.title} - ${l.registration_number}`
    }));

    return options;
  };

  const getMembers = async ({ name }) => {
    const {
      data: { searchMember }
    } = await query({
      query: SEARCH_MEMBER,
      variables: { name },
      fetctPolicy: 'no-cache'
    });

    const options = searchMember.map(l => ({
      value: l.id,
      label: `${l.name} - ${l.class.grade}`
    }));

    return options;
  };

  const loadOptions = async (inputValue, type) => {
    if (type === 'book') {
      return getBooks({
        title: inputValue
      });
    }

    if (type === 'member') {
      return getMembers({
        name: inputValue
      });
    }

    return null;
  };

  const handleInputChange = (newValue, type) => {
    if (type === 'book') {
      const inputValue = newValue.replace(/\W/g, '');
      return inputValue;
    }

    if (type === 'member') {
      const inputValue = newValue.replace(/\W/g, '');
      return inputValue;
    }

    return null;
  };

  const handleClose = () => {
    setOpen(false);
  };

  const getDetailBook = async id => {
    const {
      data: { book }
    } = await query({
      query: BOOK,
      variables: { id },
      fetctPolicy: 'no-cache'
    });

    console.log('book', book);
    return setBook({
      id: book.id,
      title: book.title,
      registration_number: book.registration_number,
      code_number: book.code_number,
      quantity: book.quantity,
      city: book.city,
      year: book.year,
      cover_url: book.cover_url,
      created_at: moment(book.created_at).format('DD - MM - YYYY'),
      publisher: book.publisher ? book.publisher.name : null,
      author: book.author ? book.author.name : null,
      bookshelf: book.bookshelf ? book.bookshelf.name : null
    });
  };

  return (
    <div>
      <MyBreadcrumbs
        primary={{ title: 'Beranda', url: '/' }}
        secondary={{ title: 'Transaksi', url: '/borrow' }}
        tertiary={{ title: 'Peminjaman' }}
      />
      <Grid container spacing={0} justify="center">
        <Grid item xs={12} sm={12} md={6} lg={6}>
          <Formik
            initialValues={{
              member: '',
              book: ''
            }}
            validationSchema={Yup.object().shape({
              member: Yup.string().required('Harus diisi'),
              book: Yup.string().required('Harus diisi')
            })}
            onSubmit={async ({ book, member }, { resetForm }) => {
              try {
                setGlobalLoading(true);
                const {
                  data: { createBorrow }
                } = await mutate({
                  mutation: CREATE_BORROW,
                  variables: {
                    data: {
                      officer_id: cookie.get('id'),
                      member_id: member.value,
                      book_id: book.value,
                      borrow_at: new Date()
                    }
                  },
                  fetchPolicy: 'no-cache',
                  onError: error => {
                    console.log('==> Error execute mutation', error);
                  }
                });
                setSnack({
                  variant: 'success',
                  message: 'Peminjaman Buku berhasil',
                  opened: true
                });
                setGlobalLoading(false);
                console.log('createBorrow', createBorrow);
                resetForm();
                history.replace('/borrow');
                window.location.reload();
              } catch (error) {
                setGlobalLoading(false);
                setError(setSnack, error);
              }
            }}
          >
            {({
              errors,
              touched,
              handleSubmit,
              values,
              handleChange,
              setFieldValue
            }) => (
              <form noValidate>
                <Paper className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="officer"
                        label="Nama Petugas"
                        disabled
                        value={cookie.get('name')}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <TextField
                        margin="normal"
                        fullWidth
                        id="date"
                        label="Tanggal Peminjaman"
                        disabled
                        value={moment().format('DD/MM/YYYY')}
                      />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6} lg={6}>
                      <InputLabel shrink className={classes.labelAsyncSelect}>
                        Nama Anggota *
                      </InputLabel>

                      <AsyncSelect
                        id="member"
                        margin="normal"
                        name="member"
                        placeholder="Nama Anggota"
                        loadOptions={e => loadOptions(e, 'member')}
                        defaultOptions
                        values={values.member}
                        autoFocus
                        onInputChange={e => handleInputChange(e, 'member')}
                        onChange={e => {
                          setFieldValue('member', e);
                        }}
                      />
                      {errors.member && touched.member && (
                        <div style={{ color: 'red' }}>{errors.member}</div>
                      )}
                    </Grid>
                    <Grid item xs={11} sm={11} md={5} lg={5}>
                      <InputLabel shrink className={classes.labelAsyncSelect}>
                        Judul Buku *
                      </InputLabel>

                      <AsyncSelect
                        id="book"
                        margin="normal"
                        name="book"
                        placeholder="Judul Buku"
                        loadOptions={e => loadOptions(e, 'book')}
                        defaultOptions
                        values={values.book}
                        onInputChange={e => handleInputChange(e, 'book')}
                        onChange={e => {
                          setFieldValue('book', e);
                          getDetailBook(e.value);
                        }}
                      />
                      {errors.book && touched.book && (
                        <div style={{ color: 'red' }}>{errors.book}</div>
                      )}
                    </Grid>
                    <Grid
                      item
                      xs={1}
                      sm={1}
                      md={1}
                      lg={1}
                      style={{
                        marginTop: '8px',
                        textAlign: 'center'
                      }}
                    >
                      {book.id && (
                        <Tooltip title="Detail Buku">
                          <IconButton
                            aria-label="delete"
                            onClick={() => setOpen(true)}
                          >
                            <InfoIcon />
                          </IconButton>
                        </Tooltip>
                      )}
                    </Grid>
                    <Grid
                      container
                      direction="row"
                      justify="flex-end"
                      alignItems="baseline"
                      style={{
                        marginTop: '10px'
                      }}
                    >
                      <Button
                        onClick={handleSubmit}
                        variant="contained"
                        color="primary"
                        style={{
                          marginRight: 10
                        }}
                      >
                        Simpan
                      </Button>
                      <Button
                        onClick={() => history.replace('/borrow')}
                        color="primary"
                      >
                        Kembali
                      </Button>
                    </Grid>
                  </Grid>
                </Paper>
              </form>
            )}
          </Formik>
        </Grid>
      </Grid>
      <MyModal
        open={open}
        handleClose={handleClose}
        data={book}
        type="Buku"
        mappingRows={{
          id: 'ID Buku',
          title: 'Judul Buku',
          registration_number: 'Nomor Registrasi',
          code_number: 'Kode Buku',
          quantity: 'Jumlah',
          city: 'Kota',
          year: 'Tahun',
          cover_url: 'Sampul',
          created_at: 'Tanggal Dibuat',
          publisher: 'Penerbit',
          author: 'Pengarang',
          bookshelf: 'Rak Buku'
        }}
      />
    </div>
  );
};

export default ClassForm;
