import React from 'react';
import Cookies from 'universal-cookie';
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  Route
} from 'react-router-dom';
import MySnack from '../components/MySnack';
import MyLoading from '../components/MyLoading';
import SignIn from '../components/SignIn';
import Admin from '../components/Admin';
import MyVerification from '../components/Custom/MyVerification';
import VisitorPage from '../components/Visitor/VisitorPage';

const cookies = new Cookies();
const PrivateRoute = prop => (
  <div>
    <Route
      path="/"
      render={props =>
        cookies.get('token') ? (
          <Admin {...prop} {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location }
            }}
          />
        )
      }
    />
  </div>
);

const MyRouter = greaterProps => {
  return (
    <Router>
      <Switch>
        <Route
          exact
          path="/login"
          render={props => (
            <SignIn query={greaterProps.client.query} {...props} />
          )}
        />
        <Route
          exact
          path="/verification/:uuid"
          render={props => (
            <MyVerification
              query={greaterProps.client.query}
              mutate={greaterProps.client.mutate}
              {...props}
            />
          )}
        />
        <Route
          exact
          path="/visitor"
          render={props => (
            <VisitorPage
              query={greaterProps.client.query}
              mutate={greaterProps.client.mutate}
              {...props}
            />
          )}
        />
        <PrivateRoute
          query={greaterProps.client.query}
          mutate={greaterProps.client.mutate}
        />
      </Switch>
      <MySnack {...greaterProps.snack} />
      {greaterProps.globalLoading && <MyLoading />}
    </Router>
  );
};

export default MyRouter;
