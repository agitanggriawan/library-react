import moment from 'moment-timezone';
import lodash from 'lodash';

export const setError = (setSnack, error) => {
  if (error.graphQLErrors && error.graphQLErrors.length > 0)
    setSnack({
      variant: 'error',
      message: error.graphQLErrors[0].message,
      opened: true
    });
  if (error.networkError && error.networkError.length > 0)
    setSnack({
      variant: 'error',
      message: error.networkError[0].message,
      opened: true
    });
};

export const getSchoolDays = (startDate, endDate, holiday) => {
  const fromDate = moment(startDate);
  const toDate = moment(endDate);
  const diff = toDate.diff(fromDate, 'days');
  const range = [];
  for (let i = 0; i <= diff; i++) {
    range.push(
      moment(startDate)
        .add(i, 'days')
        .format('YYYY-MM-DD')
    );
  }
  const diffDate = lodash.difference(range, holiday);
  const fixedDiffDate = diffDate.filter(x => moment(x).day() !== 0);

  return fixedDiffDate;
};
