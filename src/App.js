import React, { useState } from 'react';
// import ApolloClient from 'apollo-boost';
import { ApolloClient } from 'apollo-client';
import { onError } from 'apollo-link-error';
import { createUploadLink as CreateUploadLink } from 'apollo-upload-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloProvider, ApolloConsumer } from '@apollo/react-hooks';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

import { GlobalProvider } from './utils/GlobalContext';
import './App.css';
import MyRouter from './utils/MyRouter';

const uri =
  process.env.NODE_PATH === 'development'
    ? 'http://localhost:5000/data'
    : 'https://library-gql.herokuapp.com/data';

const httpLink = new CreateUploadLink({
  uri,
});

const linkError = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) console.log('graphQLErrors App.js: ', graphQLErrors);
  if (networkError) console.log('networkError: ', networkError);
});

// const uploadLink = authAndError.concat(createUploadLink);
const client = new ApolloClient({
  link: linkError.concat(httpLink),
  cache: new InMemoryCache(),
});
console.log('process.env.NODE_PATH', process.env);
function App() {
  const [snack, setSnack] = useState({
    variant: 'success',
    message: null,
    opened: false,
  });
  const [globalLoading, setGlobalLoading] = useState(false);
  const theme = createMuiTheme({});

  return (
    <ThemeProvider theme={theme}>
      <GlobalProvider value={{ setSnack, setGlobalLoading }}>
        <ApolloProvider client={client}>
          <ApolloConsumer>
            {(client) => (
              <MyRouter
                client={client}
                snack={snack}
                globalLoading={globalLoading}
              />
            )}
          </ApolloConsumer>
        </ApolloProvider>
      </GlobalProvider>
    </ThemeProvider>
  );
}

export default App;
