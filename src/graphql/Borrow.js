import gql from 'graphql-tag';

export const BORROWS = gql`
  query borrows($class_id: ID) {
    borrows(class_id: $class_id) {
      id
      officer_id
      member_id
      book_id
      is_borrowed
      payment_penalty
      borrow_at
      max_return
      return_at
      created_at
      updated_at

      officer {
        id
        name
        role
        email
        address
        phone
        password
        created_at
        updated_at
      }
      member {
        id
        class_id
        name
        address
        gender
        phone
        birthdate
        photo
        class {
          id
          grade
          created_at
          updated_at
        }
        created_at
        updated_at
      }
      book {
        id
        publisher_id
        author_id
        bookshelf_id
        registration_number
        code_number
        title
        quantity
        city
        year
        cover_url
        created_at
        updated_at
        publisher {
          id
          name
          created_at
          updated_at
        }
        author {
          id
          name
          created_at
          updated_at
        }
        bookshelf {
          id
          name
          created_at
          updated_at
        }
      }
    }
  }
`;

export const BORROW = gql`
  query author($id: ID!) {
    author(id: $id) {
      id
      officer_id
      member_id
      book_id
      is_borrowed
      payment_penalty
      borrow_at
      max_return
      return_at
      created_at
      updated_at

      officer {
        id
        name
        role
        email
        address
        phone
        password
        created_at
        updated_at
      }
      member {
        id
        class_id
        name
        address
        gender
        phone
        birthdate
        photo
        class {
          id
          grade
          created_at
          updated_at
        }
        created_at
        updated_at
      }
      book {
        id
        publisher_id
        author_id
        bookshelf_id
        registration_number
        code_number
        title
        quantity
        city
        year
        cover_url
        created_at
        updated_at
        publisher {
          id
          name
          created_at
          updated_at
        }
        author {
          id
          name
          created_at
          updated_at
        }
        bookshelf {
          id
          name
          created_at
          updated_at
        }
      }
    }
  }
`;

export const RETURNS = gql`
  query returns {
    returns {
      id
      officer_id
      member_id
      book_id
      is_borrowed
      payment_penalty
      borrow_at
      max_return
      return_at
      created_at
      updated_at

      officer {
        id
        name
        role
        email
        address
        phone
        password
        created_at
        updated_at
      }
      member {
        id
        class_id
        name
        address
        gender
        phone
        birthdate
        photo
        class {
          id
          grade
          created_at
          updated_at
        }
        created_at
        updated_at
      }
      book {
        id
        publisher_id
        author_id
        bookshelf_id
        registration_number
        code_number
        title
        quantity
        city
        year
        cover_url
        created_at
        updated_at
        publisher {
          id
          name
          created_at
          updated_at
        }
        author {
          id
          name
          created_at
          updated_at
        }
        bookshelf {
          id
          name
          created_at
          updated_at
        }
      }
    }
  }
`;

export const CREATE_BORROW = gql`
  mutation createBorrow($data: InBorrow!) {
    createBorrow(data: $data) {
      id
      officer_id
      member_id
      book_id
      is_borrowed
      payment_penalty
      borrow_at
      max_return
      return_at
      created_at
      updated_at

      officer {
        id
        name
        role
        email
        address
        phone
        password
        created_at
        updated_at
      }
      member {
        id
        class_id
        name
        address
        gender
        phone
        birthdate
        photo
        class {
          id
          grade
          created_at
          updated_at
        }
        created_at
        updated_at
      }
      book {
        id
        publisher_id
        author_id
        bookshelf_id
        registration_number
        code_number
        title
        quantity
        city
        year
        cover_url
        created_at
        updated_at
        publisher {
          id
          name
          created_at
          updated_at
        }
        author {
          id
          name
          created_at
          updated_at
        }
        bookshelf {
          id
          name
          created_at
          updated_at
        }
      }
    }
  }
`;

export const UPDATE_BORROW = gql`
  mutation updateBorrow($id: ID!, $data: InBorrow!) {
    updateBorrow(id: $id, data: $data) {
      id
      officer_id
      member_id
      book_id
      is_borrowed
      payment_penalty
      borrow_at
      max_return
      return_at
      created_at
      updated_at

      officer {
        id
        name
        role
        email
        address
        phone
        password
        created_at
        updated_at
      }
      member {
        id
        class_id
        name
        address
        gender
        phone
        birthdate
        photo
        class {
          id
          grade
          created_at
          updated_at
        }
        created_at
        updated_at
      }
      book {
        id
        publisher_id
        author_id
        bookshelf_id
        registration_number
        code_number
        title
        quantity
        city
        year
        cover_url
        created_at
        updated_at
        publisher {
          id
          name
          created_at
          updated_at
        }
        author {
          id
          name
          created_at
          updated_at
        }
        bookshelf {
          id
          name
          created_at
          updated_at
        }
      }
    }
  }
`;

export const RETURN_BOOK = gql`
  mutation returnBook($id: ID!, $penalty: Int!) {
    returnBook(id: $id, penalty: $penalty) {
      id
      officer_id
      member_id
      book_id
      is_borrowed
      payment_penalty
      borrow_at
      max_return
      return_at
      created_at
      updated_at

      officer {
        id
        name
        role
        email
        address
        phone
        password
        created_at
        updated_at
      }
      member {
        id
        class_id
        name
        address
        gender
        phone
        birthdate
        photo
        class {
          id
          grade
          created_at
          updated_at
        }
        created_at
        updated_at
      }
      book {
        id
        publisher_id
        author_id
        bookshelf_id
        registration_number
        code_number
        title
        quantity
        city
        year
        cover_url
        created_at
        updated_at
        publisher {
          id
          name
          created_at
          updated_at
        }
        author {
          id
          name
          created_at
          updated_at
        }
        bookshelf {
          id
          name
          created_at
          updated_at
        }
      }
    }
  }
`;
