import gql from 'graphql-tag';

export const TOTAL_PUBLISHER = gql`
  query totalPublisher {
    totalPublisher {
      count
    }
  }
`;
