import gql from 'graphql-tag';

export const HOLIDAYS = gql`
  query holidays {
    holidays {
      id
      date_off
      name
      created_at
      updated_at
    }
  }
`;

export const HOLIDAY = gql`
  query holiday($id: ID!) {
    holiday(id: $id) {
      id
      date_off
      name
      created_at
      updated_at
    }
  }
`;

export const CREATE_HOLIDAY = gql`
  mutation createHoliday($data: InHoliday!) {
    createHoliday(data: $data) {
      id
      date_off
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_HOLIDAY = gql`
  mutation updateHoliday($id: ID!, $data: InHoliday!) {
    updateHoliday(id: $id, data: $data) {
      id
      date_off
      name
      created_at
      updated_at
    }
  }
`;
