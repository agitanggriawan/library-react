import gql from 'graphql-tag';

export const AUTHORS = gql`
  query authors {
    authors {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const AUTHOR = gql`
  query author($id: ID!) {
    author(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const SEARCH_AUTHOR = gql`
  query searchAuthor($name: String!) {
    searchAuthor(name: $name) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const CREATE_AUTHOR = gql`
  mutation createAuthor($data: InAuthor!) {
    createAuthor(data: $data) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_AUTHOR = gql`
  mutation updateAuthor($id: ID!, $data: InAuthor!) {
    updateAuthor(id: $id, data: $data) {
      id
      name
      created_at
      updated_at
    }
  }
`;
