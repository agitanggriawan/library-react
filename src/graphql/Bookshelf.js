import gql from 'graphql-tag';

export const BOOKSHELFS = gql`
  query bookshelfs {
    bookshelfs {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const BOOKSHELF = gql`
  query bookshelf($id: ID!) {
    bookshelf(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const SEARCH_BOOKSHELF = gql`
  query searchBookshelf($name: String!) {
    searchBookshelf(name: $name) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const CREATE_BOOKSHELF = gql`
  mutation createBookshelf($data: InBookshelf!) {
    createBookshelf(data: $data) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_BOOKSHELF = gql`
  mutation updateBookshelf($id: ID!, $data: InBookshelf!) {
    updateBookshelf(id: $id, data: $data) {
      id
      name
      created_at
      updated_at
    }
  }
`;
