import gql from 'graphql-tag';

export const MEMBERS = gql`
  query members {
    members {
      id
      class_id
      name
      email
      address
      gender
      status
      phone
      birthdate
      photo
      registration_token
      class {
        id
        grade
        created_at
        updated_at
      }
      created_at
      updated_at
    }
  }
`;

export const MEMBER = gql`
  query member($id: ID!) {
    member(id: $id) {
      id
      class_id
      name
      email
      address
      gender
      status
      phone
      birthdate
      photo
      registration_token
      class {
        id
        grade
        created_at
        updated_at
      }
      created_at
      updated_at
    }
  }
`;

export const SEARCH_MEMBER = gql`
  query searchMember($name: String!) {
    searchMember(name: $name) {
      id
      class_id
      name
      email
      address
      gender
      status
      phone
      birthdate
      photo
      registration_token
      class {
        id
        grade
        created_at
        updated_at
      }
      created_at
      updated_at
    }
  }
`;

export const CREATE_MEMBER = gql`
  mutation createMember($data: InMember!) {
    createMember(data: $data) {
      id
      class_id
      name
      email
      address
      gender
      status
      phone
      birthdate
      photo
      registration_token
      class {
        id
        grade
        created_at
        updated_at
      }
      created_at
      updated_at
    }
  }
`;

export const UPDATE_MEMBER = gql`
  mutation updateMember($id: ID!, $data: InMember!) {
    updateMember(id: $id, data: $data) {
      id
      class_id
      name
      email
      address
      gender
      status
      phone
      birthdate
      photo
      registration_token
      class {
        id
        grade
        created_at
        updated_at
      }
      created_at
      updated_at
    }
  }
`;

export const VERIFY_MEMBER = gql`
  mutation verifyMember($registration_token: String!) {
    verifyMember(registration_token: $registration_token) {
      id
      class_id
      name
      email
      address
      gender
      status
      phone
      birthdate
      photo
      registration_token
      created_at
      updated_at
    }
  }
`;
