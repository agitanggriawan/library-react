import gql from 'graphql-tag';

export const CLASSES = gql`
  query classes {
    classes {
      id
      grade
      created_at
      updated_at
    }
  }
`;

export const CLASS = gql`
  query class($id: ID!) {
    class(id: $id) {
      id
      grade
      created_at
      updated_at
    }
  }
`;

export const CREATE_CLASS = gql`
  mutation createClass($data: InClass!) {
    createClass(data: $data) {
      id
      grade
      created_at
      updated_at
    }
  }
`;

export const UPDATE_CLASS = gql`
  mutation updateClass($id: ID!, $data: InClass!) {
    updateClass(id: $id, data: $data) {
      id
      grade
      created_at
      updated_at
    }
  }
`;
