import gql from 'graphql-tag';

export const AUTH = gql`
  query authenticate($email: String!, $password: String!) {
    authenticate(email: $email, password: $password) {
      id
      role
      name
      email
      token
    }
  }
`;

export const OFFICERS = gql`
  query officers {
    officers {
      id
      name
      role
      email
      address
      phone
      password
      created_at
      updated_at
    }
  }
`;

export const OFFICER = gql`
  query officer($id: ID!) {
    officer(id: $id) {
      id
      name
      role
      email
      address
      phone
      password
      created_at
      updated_at
    }
  }
`;

export const SEARCH_OFFICER = gql`
  query searchOfficer($name: String!) {
    searchOfficer(name: $name) {
      id
      name
      role
      email
      address
      phone
      password
      created_at
      updated_at
    }
  }
`;

export const CREATE_OFFICER = gql`
  mutation createOfficer($data: InOfficer!) {
    createOfficer(data: $data) {
      id
      name
      role
      email
      address
      phone
      password
      created_at
      updated_at
    }
  }
`;

export const UPDATE_OFFICER = gql`
  mutation updateOfficer($id: ID!, $data: InOfficer!) {
    updateOfficer(id: $id, data: $data) {
      id
      name
      role
      email
      address
      phone
      password
      created_at
      updated_at
    }
  }
`;
