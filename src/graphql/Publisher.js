import gql from 'graphql-tag';

export const PUBLISHERS = gql`
  query publishers {
    publishers {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const PUBLISHER = gql`
  query publisher($id: ID!) {
    publisher(id: $id) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const SEARCH_PUBLISHER = gql`
  query searchPublisher($name: String!) {
    searchPublisher(name: $name) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const CREATE_PUBLISHER = gql`
  mutation createPublisher($data: InPublisher!) {
    createPublisher(data: $data) {
      id
      name
      created_at
      updated_at
    }
  }
`;

export const UPDATE_PUBLISHER = gql`
  mutation updatePublisher($id: ID!, $data: InPublisher!) {
    updatePublisher(id: $id, data: $data) {
      id
      name
      created_at
      updated_at
    }
  }
`;
