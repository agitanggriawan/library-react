import gql from 'graphql-tag';

export const BOOKS = gql`
  query books {
    books {
      id
      uuid
      publisher_id
      author_id
      bookshelf_id
      registration_number
      code_number
      title
      quantity
      city
      year
      cover_url
      created_at
      updated_at
      publisher {
        id
        name
        created_at
        updated_at
      }
      author {
        id
        name
        created_at
        updated_at
      }
      bookshelf {
        id
        name
        created_at
        updated_at
      }
    }
  }
`;

export const BOOK = gql`
  query book($id: ID!) {
    book(id: $id) {
      id
      uuid
      publisher_id
      author_id
      bookshelf_id
      registration_number
      code_number
      title
      quantity
      city
      year
      cover_url
      created_at
      updated_at
      publisher {
        id
        name
        created_at
        updated_at
      }
      author {
        id
        name
        created_at
        updated_at
      }
      bookshelf {
        id
        name
        created_at
        updated_at
      }
    }
  }
`;

export const SEARCH_BOOK = gql`
  query searchBook($title: String!) {
    searchBook(title: $title) {
      id
      uuid
      publisher_id
      author_id
      bookshelf_id
      registration_number
      code_number
      title
      quantity
      city
      year
      cover_url
      created_at
      updated_at
    }
  }
`;

export const CREATE_BOOK = gql`
  mutation createBook($data: InBook!) {
    createBook(data: $data) {
      id
      uuid
      publisher_id
      author_id
      bookshelf_id
      registration_number
      code_number
      title
      quantity
      city
      year
      cover_url
      created_at
      updated_at
    }
  }
`;

export const UPDATE_BOOK = gql`
  mutation updateBook($id: ID!, $data: InBook!) {
    updateBook(id: $id, data: $data) {
      id
      uuid
      publisher_id
      author_id
      bookshelf_id
      registration_number
      code_number
      title
      quantity
      city
      year
      cover_url
      created_at
      updated_at
    }
  }
`;
